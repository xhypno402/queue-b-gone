#!/bin/bash

function sendkeys() {
    echo "$@" | sudo /home/pi/Queue-B-Gone/sendkeys /dev/hidg0 keyboard
}

function screen_right() {
    logger -p local6.notice -t "Afk-What" "Moving to right screen."
    sendkeys left-ctrl left-meta right
}

function screen_left() {
    logger -p local6.notice -t "Afk-What" "Moving to left screen."
    sendkeys left-ctrl left-meta left
}

function logout() {
    logger -p local6.notice -t "Afk-What" "Logging Character out."
    sendkeys enter
    sendkeys slash
    sendkeys l o g o u t
    sendkeys enter
    
    if [ $IN_CITY ]; then
        logger -p local6.notice -t "Afk-What" "Waiting 10 seconds for logout to finish."
        sleep 11
    else
        logger -p local6.notice -t "Afk-What" "Waiting 30 seconds for logout to finish."
        sleep 31
    fi
}

function login() {
    logger -p local6.notice -t "Afk-What" "Logging Character in."
    sendkeys enter
    sleep 2
}

if [ ! -f sendkeys ]; then
    gcc -o sendkeys sendkeys.c
fi

logger -p local6.notice -t "Afk-What" "Queue-B-Gone, Loaded Global Config"
source /etc/queue-b-gone

rm /tmp/queue-b-gone-env
curl https://gitlab.com/xhypno402/queue-b-gone-env/-/raw/master/${ENV_FILE} -o /tmp/queue-b-gone-env --silent

logger -p local6.notice -t "Afk-What" "Queue-B-Gone, Loaded Env: `cat /tmp/queue-b-gone-env`"
source /tmp/queue-b-gone-env

logger -p local6.notice -t "Afk-What" "Queue-B-Gone, starting in 10 seconds."
sleep 10

while true
do
    logout
    login

    if [ $MON_TWO ]; then
    	screen_right
    	logout
    	login
	screen_left
    fi

    if [ $MON_THREE ]; then
    	screen_right
	screen_right
    	logout
    	login
    	screen_left
    	screen_left
    fi

    TI=$(((RANDOM%420+901)))
    logger -p local6.notice -t "Afk-What" "Sleeping for $TI seconds."
    sleep $TI
done

