#!/bin/bash
### BEGIN INIT INFO
# Provides:          afk-what
# Required-Start:    $all
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: your description here
### END INIT INFO

PID=/var/run/afk-what.pid

case "$1" in 
start)
   /home/pi/Queue-B-Gone/afk-what.sh &
   echo $!>$PID
   ;;
stop)
   kill `cat $PID`
   rm $PID 
   ;;
restart)
   $0 stop
   $0 start
   ;;
status)
   if [ -e $PID ]; then
      echo afk-what.sh is running, pid=`cat $PID`
   else
      echo afk-what.sh is NOT running
      exit 1
   fi
   ;;
*)
   echo "Usage: $0 {start|stop|status|restart}"
esac

exit 0 
